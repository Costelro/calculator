package calculator;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;
import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.HashMap;

public class Main extends Application
{

    private static String operand1 = "";
    private static String operator = "";
    private static String operand2 = "";
    private static String result   = "0";
    private static boolean isResultOfFinishedOperation = false;


    @Override
    public void start(Stage primaryStage) throws Exception
    {
        /* ***************************************************************
        *  The basic layout and formatting of the calculator's interface
        *  and the visual formatting of the calculator's "display".
        ****************************************************************** */

        GridPane calcGrid = new GridPane();
        calcGrid.setStyle("-fx-background-color: #F0F8FF;");
        calcGrid.setAlignment(Pos.CENTER);
        calcGrid.setVgap(10);
        calcGrid.setHgap(10);
        calcGrid.setPadding(new Insets(10, 10, 10, 10));

        Rectangle backgroundDisplay = new Rectangle();
        backgroundDisplay.setWidth(325);
        backgroundDisplay.setHeight(50);
        backgroundDisplay.setArcWidth(10);
        backgroundDisplay.setArcHeight(10);
        backgroundDisplay.setStroke(Color.DARKGRAY);
        backgroundDisplay.setStrokeWidth(2);
        backgroundDisplay.setFill(Color.CORNSILK);
        calcGrid.add(backgroundDisplay, 0, 0, 5, 1);

        Text textDisplay = new Text();
        textDisplay.setText(Main.result);
        textDisplay.setFont(Font.font("Arial", FontWeight.BOLD, 25));
        textDisplay.setFill(Color.DARKBLUE);
        textDisplay.setWrappingWidth(300);
        textDisplay.setTextAlignment(TextAlignment.RIGHT);
        calcGrid.add(textDisplay, 0, 0, 5, 1);


        /* *******************************************************************
        *  For each calculator button, we define:
        *  - its label, as <String> = "0"..."9", "+", "-", "*", "/";
        *  - its position & size in the visual grid,
        *    inside an <int[]>: btnColumn, btnRow, btnColumnSpan, btnRowSpan.
        ********************************************************************** */

        HashMap<String, int[]> buttonLabels = new HashMap<>();
        int[] btn0 = {0, 4, 1, 1};
            buttonLabels.put("0", btn0);
        int[] btn1 = {0, 3, 1, 1};
            buttonLabels.put("1", btn1);
        int[] btn2 = {1, 3, 1, 1};
            buttonLabels.put("2", btn2);
        int[] btn3 = {2, 3, 1, 1};
            buttonLabels.put("3", btn3);
        int[] btn4 = {0, 2, 1, 1};
            buttonLabels.put("4", btn4);
        int[] btn5 = {1, 2, 1, 1};
            buttonLabels.put("5", btn5);
        int[] btn6 = {2, 2, 1, 1};
            buttonLabels.put("6", btn6);
        int[] btn7 = {0, 1, 1, 1};
            buttonLabels.put("7", btn7);
        int[] btn8 = {1, 1, 1, 1};
            buttonLabels.put("8", btn8);
        int[] btn9 = {2, 1, 1, 1};
            buttonLabels.put("9", btn9);
        int[] btnAdd = {3, 1, 1, 1};
            buttonLabels.put("+", btnAdd);
        int[] btnSubstract = {3, 2, 1, 1};
            buttonLabels.put("-", btnSubstract);
        int[] btnMultiply = {3, 3, 1, 1};
            buttonLabels.put("*", btnMultiply);
        int[] btnDivide = {3, 4, 1, 1};
            buttonLabels.put("/", btnDivide);
        int[] btnEqual = {1, 4, 2, 1};
        buttonLabels.put("=", btnEqual);

        for (Object label : buttonLabels.keySet())
        {
            Button button = new Button();
            button.setText(label.toString());
            button.setFont(Font.font("Arial", 30));

            // horizontal padding is bigger for the 2-column buttons
            switch (label.toString())
            {
                case "+":
                    button.setPadding(new Insets(10, 53, 10, 53));
                    button.setStyle("-fx-background-color: #F0E68C;");
                    break;
                case "-":
                    button.setPadding(new Insets(10, 57, 10, 57));
                    button.setStyle("-fx-background-color: #F0E68C;");
                    break;
                case "*":
                    button.setPadding(new Insets(10, 56, 10, 56));
                    button.setStyle("-fx-background-color: #F0E68C;");
                    break;
                case "/":
                    button.setPadding(new Insets(10, 58, 10, 58));
                    button.setStyle("-fx-background-color: #F0E68C;");
                    break;
                case "=":
                    button.setPadding(new Insets(10, 53, 10, 53));
                    button.setStyle("-fx-background-color: #DAA520;");
                    break;
                default:
                    button.setPadding(new Insets(10, 20, 10, 20));
                    break;
            }

            // buttons have different coordinates & spanning
            int btnColumn = buttonLabels.get(label)[0];
            int btnRow = buttonLabels.get(label)[1];
            int btnColumnSpan = buttonLabels.get(label)[2];
            int btnRowSpan = buttonLabels.get(label)[3];
            calcGrid.add(button, btnColumn, btnRow, btnColumnSpan, btnRowSpan);

            // 1 mouse-event handler for each button
            button.setOnMouseClicked(new EventHandler<MouseEvent>()
            {
                @Override
                public void handle(MouseEvent event)
                {
                    processInput(label.toString());    // will change: Main.result (& operands & operator, if the case)
                    textDisplay.setText(displayResult(Main.result));
                }
            });
        }


        /* *******************************************************************************
        *  Keyboard-events filter to process all planned operations.
        *  Accepted keys: numpad digits, operators & Enter, main-keyboard digits & Enter.
        ********************************************************************************** */

        calcGrid.addEventFilter(KeyEvent.KEY_PRESSED, new EventHandler<KeyEvent>() {
            public void handle(KeyEvent event)
            {
                // each processInput() call will change: Main.result (& operands & operator, if the case)
                if ( (event.getCode() == KeyCode.DIGIT1) || (event.getCode() == KeyCode.NUMPAD1) )
                {
                    processInput("1");
                }
                else if ( (event.getCode() == KeyCode.DIGIT2) || (event.getCode() == KeyCode.NUMPAD2) )
                {
                    processInput("2");
                }
                else if ( (event.getCode() == KeyCode.DIGIT3) || (event.getCode() == KeyCode.NUMPAD3) )
                {
                    processInput("3");
                }
                else if ( (event.getCode() == KeyCode.DIGIT4) || (event.getCode() == KeyCode.NUMPAD4) )
                {
                    processInput("4");
                }
                else if ( (event.getCode() == KeyCode.DIGIT5) || (event.getCode() == KeyCode.NUMPAD5) )
                {
                    processInput("5");
                }
                else if ( (event.getCode() == KeyCode.DIGIT6) || (event.getCode() == KeyCode.NUMPAD6) )
                {
                    processInput("6");
                }
                else if ( (event.getCode() == KeyCode.DIGIT7) || (event.getCode() == KeyCode.NUMPAD7) )
                {
                    processInput("7");
                }
                else if ( (event.getCode() == KeyCode.DIGIT8) || (event.getCode() == KeyCode.NUMPAD8) )
                {
                    processInput("8");
                }
                else if ( (event.getCode() == KeyCode.DIGIT9) || (event.getCode() == KeyCode.NUMPAD9) )
                {
                    processInput("9");
                }
                else if ( (event.getCode() == KeyCode.DIGIT0) || (event.getCode() == KeyCode.NUMPAD0) )
                {
                    processInput("0");
                }
                else if (event.getCode() == KeyCode.ADD)
                {
                    processInput("+");
                }
                else if (event.getCode() == KeyCode.SUBTRACT)
                {
                    processInput("-");
                }
                else if (event.getCode() == KeyCode.MULTIPLY)
                {
                    processInput("*");
                }
                else if (event.getCode() == KeyCode.DIVIDE)
                {
                    processInput("/");
                }
                else if (event.getCode() == KeyCode.ENTER)
                {
                    processInput("=");
                }

                textDisplay.setText(displayResult(Main.result));
                event.consume();
            }
        });


        Scene calculatorScene = new Scene(calcGrid, 360, 380);

        primaryStage.setTitle("Calculator Java-toy (v1)");
        primaryStage.setScene(calculatorScene);
        primaryStage.show();
    }

    private void processInput(String text)
    {
        // determine the 2 operands and operator & call the computing method as needed
        switch (text)
        {
            case "+":
            case "-":
            case "*":
            case "/":
                if ( "0".equals(Main.result) || "(error: div. by zero!)".equals(Main.result) )
                {
//                    System.out.println(">>here1 <- "+operand1+"; "+operator+"; "+operand2+"; "+isResultOfFinishedOperation);
                    Main.operand1 = "0";
                    Main.operator = text;
                    Main.operand2 = "";
                    Main.result = Main.operand1;
                    isResultOfFinishedOperation = false;
                    break;
                }
                else if (Main.operand1.length() >= 1)
                {
                    if ( (Main.operator.length() == 1) && (Main.operand2.length() >= 1) )
                    {
//                        System.out.println(">>here2 <- "+operand1+"; "+operator+"; "+operand2+"; "+isResultOfFinishedOperation);
                        Main.result = computeResult(Main.operand1, Main.operator, Main.operand2);
                        isResultOfFinishedOperation = true;
                        Main.operand1 = Main.result;
                        Main.operator = text;
                        Main.operand2 = "";
                        break;
                    }
                    else if ( ((Main.operator.length() == 1) && (Main.operand2.length() == 0))
                              || Main.operator.length() == 0 )
                    {
//                        System.out.println(">>here3 <- "+operand1+"; "+operator+"; "+operand2+"; "+isResultOfFinishedOperation);
                        Main.operator = text;
                        Main.result = Main.operand1;
                        isResultOfFinishedOperation = false;
                        break;
                    }
                }

            case "=":
                if ( ("0".equals(Main.result) && Main.operator.length() == 0)
                     || "(error: div. by zero!)".equals(Main.result) )
                {
//                    System.out.println(">>here4 <- "+operand1+"; "+operator+"; "+operand2+"; "+isResultOfFinishedOperation);
                    Main.operand1 = "0";
                    Main.operator = "";
                    Main.operand2 = "";
                    Main.result = Main.operand1;
                    isResultOfFinishedOperation = false;
                    break;
                }
                else if (Main.operand1.length() >= 1)
                {
                    if ( (Main.operator.length() == 1) && (Main.operand2.length() >= 1) )
                    {
//                        System.out.println(">>here5 <- "+operand1+"; "+operator+"; "+operand2+"; "+isResultOfFinishedOperation);
                        Main.result = computeResult(Main.operand1, Main.operator, Main.operand2);
                        isResultOfFinishedOperation = true;
                        Main.operand1 = Main.result;
                        Main.operator = "";
                        Main.operand2 = "";
                        break;
                    }
                    else
                    {
//                        System.out.println(">>here6 <- "+operand1+"; "+operator+"; "+operand2+"; "+isResultOfFinishedOperation);
                        Main.operator = "";
                        Main.operand2 = "";
                        Main.result = Main.operand1;
                        isResultOfFinishedOperation = true;
                        break;
                    }
                }

            default:
                if ( ("0".equals(Main.result) && Main.operator.length() == 0)
                     || "(error: div. by zero!)".equals(Main.result) )
                {
//                    System.out.println(">>here7 <- "+operand1+"; "+operator+"; "+operand2+"; "+isResultOfFinishedOperation);
                    isResultOfFinishedOperation = false;
                    Main.operand1 = text;
                    Main.operator = "";
                    Main.operand2 = "";
                    Main.result = Main.operand1;
                    break;
                }
                else if ( (Main.operand1.length() >= 1) && (Main.operator.length() == 1) )
                {
//                    System.out.println(">>here8 <- "+operand1+"; "+operator+"; "+operand2+"; "+isResultOfFinishedOperation);
                    isResultOfFinishedOperation = false;
                    if ("0".equals(operand2)) {Main.operand2 = text;}
                    else {Main.operand2 += text;}
                    Main.result = Main.operand2;
                    break;
                }
                else if ( (Main.operand1.length() >= 1) && (Main.operator.length() == 0) )
                {
                    if (isResultOfFinishedOperation)
                    {
//                        System.out.println(">>here9 <- "+operand1+"; "+operator+"; "+operand2+"; "+isResultOfFinishedOperation);
                        isResultOfFinishedOperation = false;
                        Main.operand1 = text;
                        Main.operand2 = "";
                        Main.result = Main.operand1;
                        break;
                    }
                    else
                    {
//                        System.out.println(">>here10 <- "+operand1+"; "+operator+"; "+operand2+"; "+isResultOfFinishedOperation);
                        isResultOfFinishedOperation = false;
                        Main.operand1 += text;
                        Main.operand2 = "";
                        Main.result = Main.operand1;
                        break;
                    }
                }
        }
    }


    private String computeResult(String operand1, String operator, String operand2)
    {
        if ( "/".equals(operator) && "0".equals(operand2) )
        {
//            System.out.println(">>here0 <- "+operand1+"; "+operator+"; "+operand2+"; "+isResultOfFinishedOperation);
            return "(error: div. by zero!)";
        }

        // convert operands to BigDecimal for exact divisions
        BigDecimal calculusResult = new BigDecimal(0);
        BigDecimal op1 = new BigDecimal(operand1);
        BigDecimal op2 = new BigDecimal(operand2);

//        System.out.println(">>hereFinal <- "+operand1+"; "+operator+"; "+operand2+"; "+isResultOfFinishedOperation);
        switch (operator)
        {
            case "+":
                calculusResult = op1.add(op2);
                break;
            case "-":
                calculusResult = op1.subtract(op2);
                break;
            case "*":
                calculusResult = op1.multiply(op2);
                break;
            case "/":
                calculusResult = op1.divide(op2, 10, BigDecimal.ROUND_HALF_UP);
                break;
        }

        return calculusResult.toPlainString();
    }


    private String displayResult(String result)
    {
        if ("(error: div. by zero!)".equals(result))
        { return result; }

        // implicit format, useful for most usual needs
        MathContext resultMathContext = new MathContext(20, RoundingMode.HALF_UP);
//        DecimalFormat resultFormat = new DecimalFormat("#,##0.####");
        DecimalFormat resultFormat = new DecimalFormat("#,##0.####");

        // format for extreme numbers: huge (more than 18 decimals) or tiny (absolute value < 0.0001)
        BigDecimal tempConvertedResult = new BigDecimal(result);
        BigDecimal tempAbsoluteValueOfResult = tempConvertedResult.abs(resultMathContext);
        boolean absoluteValueComparison = ( tempAbsoluteValueOfResult.compareTo(new BigDecimal(0.0001)) < 0 );
        if ( (result.length() > 20)
             || (absoluteValueComparison && !"0".equals(result)) )
        {
            System.out.println(result + "  <-- length=" + result.length());
            resultFormat = new DecimalFormat("0.###############E0");
        }

        // final formatting of the result as a string
        BigDecimal resultBigDecimal = new BigDecimal(result, resultMathContext).stripTrailingZeros();
        return resultFormat.format(resultBigDecimal);
    }


    public static void main(String[] args)
    {
        Application.launch(args);
    }
}
